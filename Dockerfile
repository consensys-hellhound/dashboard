FROM node:latest

# Create code directory
RUN mkdir /source

# Set working directory
WORKDIR /source
ADD . /source
RUN cd /source

RUN npm install

ENTRYPOINT [ "npm","run" , "start"]
