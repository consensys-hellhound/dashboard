import VmExecution from "utils/nats/VmExecution";

var uuid = require('react-native-uuid');

export default class VmExecutionRequest extends VmExecution {

  static get TYPE() {
      return "VmExecutionRequest"
  }

  constructor(code, wantedNodes) {
    super(VmExecutionRequest.TYPE, uuid.v4())
    this.wantedNodes= wantedNodes
    this.code = code
  }

}
