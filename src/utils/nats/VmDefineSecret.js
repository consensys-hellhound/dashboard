import VmExecution from "utils/nats/VmExecution";

var uuid = require('react-native-uuid');

export default class VmDefineSecret extends VmExecution {

  static get TYPE() {
      return "VmDefineSecret"
  }

  constructor(secret) {
    super(VmDefineSecret.TYPE, uuid.v4())
    this.secret = secret
  }

}
