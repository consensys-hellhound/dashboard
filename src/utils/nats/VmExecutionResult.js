import VmExecution from "utils/nats/VmExecution";

export default class VmExecutionResult extends VmExecution{

  static get TYPE() {
      return "VmExecutionResult"
  }

  static get STATUS_IN_PROGRESS() {
      return "InProgress"
  }

  static get STATUS_SUCCESS() {
      return "Success"
  }

  constructor(computationId, nodeId, output, status) {
    super(VmExecutionResult.TYPE, computationId)
    this.status = status
    this.nodeId = nodeId
    this.output = output
  }

}
