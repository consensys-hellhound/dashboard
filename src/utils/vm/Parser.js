import LoadKey from "utils/vm/bytecode/loadKey/LoadKey";
import KeyParam from "utils/vm/bytecode/loadKey/KeyParam";
import LoadRegistry from "utils/vm/bytecode/loadRegistry/LoadRegistry";
import PaillierOperation from "utils/vm/bytecode/paillierAddConstant/PaillierOperation";
import PaillierAddConstant from "utils/vm/bytecode/paillierAddConstant/PaillierAddConstant";
import PaillierMulConstant from "utils/vm/bytecode/paillierAddConstant/PaillierMulConstant";
import PaillierAddCiphers from "utils/vm/bytecode/paillierAddConstant/PaillierAddCiphers";

export default function (bytecode){
  var listIns = []
  var bytecodeByteArray = [];
  for (var i = 0; i < bytecode.length; i += 2) {
      bytecodeByteArray.push(bytecode.substr(i, 2))
  }
  while(bytecodeByteArray.length>0){
    var ins = bytecodeByteArray.shift()
    switch (ins) {
      case LoadKey.CODE:
        listIns.push(parseLoadKey(bytecodeByteArray))
        break;
      case LoadRegistry.CODE:
        listIns.push(parseLoadRegistry(bytecodeByteArray))
        break;
      case PaillierAddConstant.CODE:
        listIns.push(parsePaillier(PaillierAddConstant.CODE, PaillierAddConstant.NAME, bytecodeByteArray))
        break;
      case PaillierMulConstant.CODE:
        listIns.push(parsePaillier(PaillierMulConstant.CODE, PaillierMulConstant.NAME, bytecodeByteArray))
        break;
      case PaillierAddCiphers.CODE:
        listIns.push(parsePaillier(PaillierAddCiphers.CODE, PaillierAddCiphers.NAME, bytecodeByteArray))
        break;
      default:
    }
  }
  return listIns
}

function parseLoadKey(bytecodeByteArray){
  var slot = bytecodeByteArray.shift()
  var keyType = bytecodeByteArray.shift()
  var keyUsage = bytecodeByteArray.shift()
  var keySize = bytecodeByteArray.splice(0,2)
  return new LoadKey(slot, keyType, keyUsage, keySize, parseKeys(parseInt(keySize.join(""), 16), bytecodeByteArray))
}

function parseKeys(keySize, bytecodeByteArray){
  var keys = []
  var keyByteArray = bytecodeByteArray.splice(0,keySize)
  while(keyByteArray.length>0){
    var tagKey = keyByteArray.splice(0,4);
    var length = keyByteArray.splice(0,4);
    var value = keyByteArray.splice(0,parseInt(length.join(""),16));
    keys.push(new KeyParam(tagKey, length, value))
  }
  return keys
}

function parseLoadRegistry(bytecodeByteArray){
  var slot = bytecodeByteArray.shift()
  var entrySize = bytecodeByteArray.splice(0,2)
  var value = bytecodeByteArray.splice(0,parseInt(entrySize.join(""),16));
  return new LoadRegistry(slot, entrySize, value)
}

function parsePaillier(ins, name, bytecodeByteArray){
  var slot = bytecodeByteArray.shift()
  var cipherRegistrySlot = bytecodeByteArray.shift()
  var constantRegistrySlot = bytecodeByteArray.shift()
  var encryptedSum = bytecodeByteArray.shift()
  return new PaillierOperation(ins, name, slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum)
}
