import PaillierOperation from "utils/vm/bytecode/paillierAddConstant/PaillierOperation";

export default class PaillierMulConstant extends PaillierOperation{

  static get CODE() {
      return "06";
  }

  static get NAME() {
      return "PaillierMulConstant";
  }

  constructor(slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum ) {
    super(PaillierMulConstant.CODE, PaillierMulConstant.NAME,  slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum)
  }

}
