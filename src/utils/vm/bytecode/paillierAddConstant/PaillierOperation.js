import Instruction from "utils/vm/bytecode/Instruction";

export default class PaillierOperation extends Instruction{

  constructor(code, name, slot, firstOperandRegistrySlot, secondOperandRegistrySlot, output ) {
    super(code, name, slot)
    this.firstOperandRegistrySlot = firstOperandRegistrySlot
    this.secondOperandRegistrySlot = secondOperandRegistrySlot
    this.output = output
  }

}
