import PaillierOperation from "utils/vm/bytecode/paillierAddConstant/PaillierOperation";

export default class PaillierAddConstant extends PaillierOperation {

  static get CODE() {
      return "05";
  }

  static get NAME() {
      return "PaillierAddConstant";
  }

  constructor(slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum ) {
    super(PaillierAddConstant.CODE, PaillierAddConstant.NAME,  slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum)
  }

}
