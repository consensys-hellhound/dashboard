import PaillierOperation from "utils/vm/bytecode/paillierAddConstant/PaillierOperation";

export default class PaillierAddCiphers extends PaillierOperation{

  static get CODE() {
      return "04";
  }

  static get NAME() {
      return "PaillierAddCiphers";
  }

  constructor(slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum ) {
    super(PaillierAddCiphers.CODE, PaillierAddCiphers.NAME,  slot, cipherRegistrySlot, constantRegistrySlot, encryptedSum)
  }

}
