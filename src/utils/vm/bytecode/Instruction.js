export default class Instruction {

  constructor(code, name, slot) {
     this.code = code
     this.name = name
     this.slot = slot
     this.nodes = []
  }

}
