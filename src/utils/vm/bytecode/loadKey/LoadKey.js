import Instruction from "utils/vm/bytecode/Instruction";

export default class LoadKey extends Instruction{

  static get CODE() {
      return "01";
  }

  static get NAME() {
      return "LoadKey";
  }

  constructor(slot, keyType, keyUsage, keySize, keys ) {
    super(LoadKey.CODE, LoadKey.NAME, slot)
    this.keyType = keyType
    this.keyUsage = keyUsage
    this.keySize = keySize
    this.keys = keys
  }

}
