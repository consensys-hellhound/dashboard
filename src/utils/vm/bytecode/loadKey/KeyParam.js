export default class KeyParam {

  constructor(tag, length, value) {
    this.tag = tag
    this.length = length
    this.value = value
  }

  getName(){
    switch (this.tag.join("")) {
      case '00001001':
        return "Paillier Pub Key N"
      case '00001002':
        return "Paillier Pub Key G"
      case '00001003':
        return "Paillier Pub Key N Squared"
      default:
        return "Unknown key"
    }
  }
}
