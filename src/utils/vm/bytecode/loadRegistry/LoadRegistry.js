import Instruction from "utils/vm/bytecode/Instruction";

export default class LoadRegistry extends Instruction{

  static get CODE() {
      return "02";
  }

  static get NAME() {
      return "LoadRegistry";
  }

  constructor(slot, entrySize, value ) {
    super(LoadRegistry.CODE, LoadRegistry.NAME, slot)
    this.entrySize = entrySize
    this.value = value
  }

}
