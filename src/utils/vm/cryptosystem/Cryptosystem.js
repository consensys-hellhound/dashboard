var uuid = require('react-native-uuid');

export default class Cryptosystem {

  constructor(name, type, possibleOperations, strenghts, weaknesses, complexity, time) {
     this.uuid = uuid.v4()
     this.name = name
     this.type = type
     this.possibleOperations = possibleOperations
     this.strenghts = strenghts
     this.weaknesses = weaknesses
     this.complexity = complexity
     this.time = time
  }

}
