export default class VmConfiguration {

  constructor(useCase, mode, interactionLevel, timeConstraint, protection) {
    this.useCase = useCase
    this.mode = mode
    this.interactionLevel = interactionLevel
    this.timeConstraint = timeConstraint
    this.protection = protection
  }

}
