import HomePage from "views/HomePage/HomePage";
import DashboardPage from "views/Dashboard/DashboardPage";
import ParserPage from "views/Tools/ParserPage";
import ReputationPage from "views/Tools/ReputationPage";
import SettingsPage from "views/Tools/SettingsPage";
import ChooseSecretPage from "views/Tools/ChooseSecretPage";

var indexRoutes = [
  { path: "/DashboardPage/:code", name: "DashboardPage", component: DashboardPage },
  { path: "/DashboardPage", name: "DashboardPage", component: DashboardPage },
  { path: "/Parser", name: "ParserPage", component: ParserPage },
  { path: "/Reputation", name: "ReputationPage", component: ReputationPage },
  { path: "/Settings", name: "SettingsPage", component: SettingsPage },
  { path: "/Secret", name: "ChooseSecretPage", component: ChooseSecretPage },
  { path: "/", name: "HomePage", component: HomePage }
];

export default indexRoutes;
