import React from "react";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";
import TextField from '@material-ui/core/TextField';

// material kit react components
import Button from "components/CustomButtons/Button";

//redux
import { connect } from 'react-redux';

//broker
import VmDefineSecret from "utils/nats/VmDefineSecret";

// view
import View from "views/common/GenericView";

//style
import chooseSecretPageStyle from "assets/jss/views/dashboardPage";

var nats

class ChooseSecretPage extends View.GenericView {

  state  = {
    secret : 0
  }

  componentDidMount(){
    window.scrollTo(0, 0)

    //connect the nats
    console.log("connecting to the nats " + this.props.natsAddress)

    nats = require("websocket-nats").connect(this.props.natsAddress)

  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/dashboard-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <br/>
                <h2 className={classes.title}>Choose a secret</h2>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <TextField
                  className={classes.textField}
                  label="secret number"
                  value={this.state.secret}
                  onChange={this.handleSecretChange.bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
                  variant="outlined" />
              <br/>
              <Button
                    color="info"
                    size="lg"
                    target="_blank"
                    onClick={this.handleValidate.bind(this)}
                    rel="noopener noreferrer">Validate</Button>
            </div>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }

  handleSecretChange = event => {
    this.setState({ secret: event.target.value });
  }

  handleValidate = event => {
    nats.publish("domain.event", JSON.stringify(new VmDefineSecret(this.state.secret)));
  }

}

const mapStateToProps = (state) => {
  return {
    natsAddress: state.settingsReducer.natsAddress,
  }
}

ChooseSecretPage.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(chooseSecretPageStyle),
  connect(mapStateToProps)
)(ChooseSecretPage);
