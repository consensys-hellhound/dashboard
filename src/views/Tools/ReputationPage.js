import React from "react";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

//redux
import { connect } from 'react-redux';

// view
import View from "views/common/GenericView";

//style
import reputationPageStyle from "assets/jss/views/dashboardPage";

class ReputationPage extends View.GenericView {

  state = {
    listNodes: []
  }

  componentDidMount(){
    window.scrollTo(0, 0)
    fetch(this.props.pythiaAddress+'/node',{
      method: 'get'
    })
    .then(response => {
      return response.json()
    })
    .then(body => {
      this.setState({ listNodes: body})}
    )
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/dashboard-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <br/>
                <h2 className={classes.title}>Reputation node manager</h2>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <br/>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                      <TableCell>Node ID</TableCell>
                      <TableCell>Status</TableCell>
                      <TableCell numeric>Réputation level</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.listNodes.map((row, index) => {
                      return (
                        <TableRow key={row.NodeId}>
                          <TableCell>{row.NodeId}</TableCell>
                          <TableCell>{row.Status}</TableCell>
                          <TableCell numeric>
                            <FormControl className={classes.formControl}>
                              <Select native name={row.NodeId} value={row.ReputationScore} onChange={this.handleChange}>
                                <option value={1}>1</option>
                                <option value={2}>2</option>
                                <option value={3}>3</option>
                                <option value={4}>4</option>
                                <option value={5}>5</option>
                                <option value={6}>6</option>
                                <option value={7}>7</option>
                                <option value={8}>8</option>
                                <option value={9}>9</option>
                                <option value={10}>10</option>
                              </Select>
                            </FormControl>
                          </TableCell>
                        </TableRow>
                      )
                    })}
                  </TableBody>
                </Table>
              <br/>
            </div>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }

  handleChange = event => {
    this.state.listNodes.forEach((node, index) => {
      if(node.NodeId===event.target.name){
        node.ReputationScore=event.target.value
        this.changeNodeReputation(node)
      }
    })
    this.setState({ listNodes: this.state.listNodes });
  }

  /**
  * This method allows you to query change Reputation of a node
  *
  */
  changeNodeReputation(node){
      fetch(this.props.pythiaAddress+"/node/", {
        method: 'put',
        mode: 'cors',
        headers: {"Content-Type": "application/json",},
        body: "{\"nodeId\": \""+node.NodeId+"\",	\"reputationScore\":"+node.ReputationScore+" , \"status\": \""+node.Status+"\"}"
      })
  }

}

const mapStateToProps = (state) => {
  return {
    pythiaAddress: state.settingsReducer.pythiaAddress,
  }
}

ReputationPage.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(reputationPageStyle),
  connect(mapStateToProps)
)(ReputationPage);
