import React from "react";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from '@material-ui/core/TextField';

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";

// view
import parser  from "utils/vm/Parser"
import View from "views/common/VmView";

//style
import parserPageStyle from "assets/jss/views/dashboardPage";

class ParserPage extends View.VmView {

  state = {
    listIns: []
  };

  componentDidMount(){
    window.scrollTo(0, 0)
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/dashboard-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <br/>
                <h2 className={classes.title}>Bytecode Parser</h2>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <br/>
              <TextField
                multiline
                fullWidth
                rowsMax="4"
                onChange={this.handleBytecodeChange()}
                className={classes.textField}
                margin="normal"
                variant="outlined"
              />
              <br/>
              <br/>
              {this.renderBytecode()}
              <br/>
              <br/>
            </div>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }

  handleBytecodeChange = name => event => {
   this.setState({
     listIns: parser(event.target.value),
   })
 }

}

ParserPage.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(parserPageStyle)
)(ParserPage);
