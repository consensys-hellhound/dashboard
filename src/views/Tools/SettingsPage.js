import React from "react";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from '@material-ui/core/TextField';

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";

//redux
import { connect } from 'react-redux';
import { changeIrisAddressAction } from 'redux/actions/SettingsAction'
import { changeImmersionAddressAction } from 'redux/actions/SettingsAction'
import { changePythiaAddressAction } from 'redux/actions/SettingsAction'
import { changeNatsAddressAction } from 'redux/actions/SettingsAction'

// view
import View from "views/common/GenericView";

//style
import settingsPageStyle from "assets/jss/views/dashboardPage";

class SettingsPage extends View.GenericView {


  componentDidMount(){
    window.scrollTo(0, 0)
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/dashboard-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <br/>
                <h2 className={classes.title}>Settings</h2>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <br/>
              <form className={classes.container} noValidate autoComplete="off">
                 <TextField
                   id="immersion-addr"
                   label="Contract Immersion Address"
                   fullWidth
                   className={classes.textField}
                   value={this.props.immersionAddress}
                   onChange={this.handleChangeImmersionAddress.bind(this)}
                   margin="normal"
                 />
                 <br/>
                 <TextField
                   id="iris-addr"
                   label="Iris address"
                   fullWidth
                   value={this.props.irisAddress}
                   onChange={this.handleChangeIrisAddress.bind(this)}
                   margin="normal"
                 />
                 <br/>
                 <TextField
                   id="nats-addr"
                   label="Pythia address"
                   fullWidth
                   value={this.props.pythiaAddress}
                   onChange={this.handleChangePythiaAddress.bind(this)}
                   margin="normal"
                 />
                 <br/>
                 <TextField
                   id="nats-addr"
                   label="Nats address"
                   fullWidth
                   value={this.props.natsAddress}
                   onChange={this.handleChangeNatsAddress.bind(this)}
                   margin="normal"
                 />
              </form>
              <br/>
            </div>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }

  handleChangeIrisAddress(event) {
   this.props.changeIrisAddressAction(event.target.value)
  }

  handleChangePythiaAddress(event) {
   this.props.changePythiaAddressAction(event.target.value)
  }

  handleChangeImmersionAddress(event) {
   this.props.changeImmersionAddressAction(event.target.value)
  }

  handleChangeNatsAddress(event) {
   this.props.changeNatsAddressAction(event.target.value)
  }
}

const mapStateToProps = (state) => {
  return {
    irisAddress: state.settingsReducer.irisAddress,
    pythiaAddress: state.settingsReducer.pythiaAddress,
    immersionAddress: state.settingsReducer.immersionAddress,
    natsAddress: state.settingsReducer.natsAddress
  }
}

const mapDispatchToProps = dispatch => ({
  changeIrisAddressAction: (address) => dispatch(changeIrisAddressAction(address)),
  changePythiaAddressAction: (address) => dispatch(changePythiaAddressAction(address)),
  changeImmersionAddressAction: (address) => dispatch(changeImmersionAddressAction(address)),
  changeNatsAddressAction: (address) => dispatch(changeNatsAddressAction(address))
})

SettingsPage.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(settingsPageStyle),
  connect(mapStateToProps, mapDispatchToProps)
)(SettingsPage);
