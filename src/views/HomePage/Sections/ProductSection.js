import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material kit react components
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Button from "components/CustomButtons/Button";

import { Route } from 'react-router-dom'

//style
import productStyle from "assets/jss/views/homePageSections/productStyle";

class ProductSection extends React.Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={7}>
            <h2 className={classes.title}>A decentralized environment for trusted computation</h2>
            <br/>
            <h5 className={classes.description}>
              HellHound provides an off-chain network of nodes that interacts closely with the on-chain Ethereum nodes.
              With it you can execute any type of computation over encrypted data and verifies the integrity of these calculations using different cryptographic tools.
              It will provide for you different computation methods depending on the use case, calculations, inputs, time constraints etc.
            </h5>
            <br/>
            <Route render={({ history}) => (
              <Button
                    color="info"
                    size="lg"
                    onClick={() => { history.push('/DashboardPage') }}
                    target="_blank"
                    rel="noopener noreferrer">Try it</Button>
            )} />
          </GridItem>
        </GridContainer>
      </div>
    )
  }

  /**
  * This method displays the dashboard
  *
  */
  handleTryClick = () => {
    console.log('onClick');
    this.transitionTo('/DashboardPage');
  }
}

export default withStyles(productStyle)(ProductSection);
