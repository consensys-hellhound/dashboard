import React from "react";
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material kit react components
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";

//style
import pathStyle from "assets/jss/views/homePageSections/pathStyle";

//assets
import kerberos from "assets/img/kerberos.png";
import cerberus from "assets/img/cerberus.png";

class PathSection extends React.Component {

  render() {
    const { classes } = this.props;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );
    return (
      <div className={classes.section}>
        <h4 className={classes.description}>Choose your path : </h4>
        <div>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <Card plain>
                <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                  <img src={cerberus} alt="..." className={imageClasses} />
                </GridItem>
                <h4 className={classes.cardTitle}>
                  Hellhound node
                  <br />
                </h4>
                <CardBody>
                  <p className={classes.description}>
                    Download a Cerberus client, either to become a HellHound node
                    (which requires going through a vetting process)
                  </p>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Card plain>
                <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                  <img src={kerberos} alt="..." className={imageClasses} />
                </GridItem>
                <h4 className={classes.cardTitle}>
                  Cerberus Client
                  <br />
                </h4>
                <CardBody>
                  <p className={classes.description}>
                    If you don’t want to follow this (dark) path, run a Cerberus client.
                  </p>
                </CardBody>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
      </div>
    );
  }
}

export default withStyles(pathStyle)(PathSection);
