import React from "react";
import compose from 'recompose/compose';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";

// view
import View from "views/common/GenericView";

//style
import homePageStyle from "assets/jss/views/homePage";

// Sections for this page
import ProductSection from "./Sections/ProductSection";
import PathSection from "./Sections/PathSection";

//redux
import { connect } from 'react-redux';
import { changeDashboardStepAction  } from 'redux/actions/DashboardAction'
import { changeDashboardTokenAction  } from 'redux/actions/DashboardAction'
import { changeDashboardCryptosystemVmConfigurationAction  } from 'redux/actions/DashboardAction'

///assets
import icon from "assets/img/icon.png";

class HomePage extends View.GenericView {

  componentDidMount(){
    //reset default values
    this.props.changeDashboardTokenAction("")
    this.props.changeDashboardStepAction(0)
    this.props.changeDashboardCryptosystemVmConfigurationAction("he")
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax filter image={require("assets/img/landing-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <img src={icon} alt="..." />
                <br/>
                <h2 className={classes.title}>Hellhound project</h2>
                <h5>
                The first truly decentralized computing environment. A set of cryptographic tools to enable dapps developers to implement privacy-by-design.
                </h5>
                <br/>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <ProductSection />
            <PathSection />
            <br/><br/><br/><br/>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    step: state.dashboardReducer.step
  }
}

const mapDispatchToProps = dispatch => ({
  changeDashboardStepAction: (newStep) => dispatch(changeDashboardStepAction(newStep)),
  changeDashboardTokenAction: (newToken) => dispatch(changeDashboardTokenAction(newToken)),
  changeDashboardCryptosystemVmConfigurationAction: (newType) => dispatch(changeDashboardCryptosystemVmConfigurationAction(newType))
})


export default compose(
  withStyles(homePageStyle),
  connect(mapStateToProps, mapDispatchToProps)
)(HomePage);
