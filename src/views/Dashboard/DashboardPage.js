import React from "react";
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from "classnames";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

// material kit react components
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";

//ethereum
import getWeb3 from 'utils/web3/getWeb3'

// view
import View from "views/common/GenericView";

// Sections for this page
import FeatureSelectionSection from "./Sections/FeatureSelectionSection";
import AuthenticationSection from "./Sections/AuthenticationSection";
import FuelSection from "./Sections/FuelSection";
import LoadScriptSection from "./Sections/LoadScriptSection";
import StartComputationSection from "./Sections/StartComputationSection";

//redux
import { connect } from 'react-redux';
import { initWeb3Action } from 'redux/actions/ContractAction'

//style
import dashboardPageStyle from "assets/jss/views/dashboardPage";

class DashboardPage extends View.GenericView {

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3
    .then(results => {
      this.props.initWeb3Action(results.web3)
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  componentDidMount(){
    window.scrollTo(0, 0)
  }

  render() {
    const { classes, ...rest } = this.props;
    const steps = getSteps();
    return (
      <div>
        <Header
          color="transparent"
          brand="Hellhound"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/dashboard-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <br/>
                <h2 className={classes.title}>Cerberus Client</h2>
                <h5 style={{color:"white"}}>
                  You will be able to use the power of the blockchain to help you to do your complex and long computation.
                </h5>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <br/>
              <Stepper activeStep={this.props.step} alternativeLabel>
                {steps.map(label => {
                  return (
                    <Step key={label}>
                      <StepLabel/>
                    </Step>
                  );
                })}
              </Stepper>
              {getStepContent(this.props.step, this)}
            </div>
          </div>
        </div>
        <Footer />
        { this.renderParticle() }
      </div>
    )
  }
}

/**
* The steps of the dashboard
*
*/
function getSteps() {
  return ['Feature selection', 'Cryptosystem selection', 'Authentication', 'Fuel section', 'Start Computation'];
}

/**
* THe content of the dashboard
*
*/
function getStepContent(index, instance) {
  const { classes } = instance.props;
  console.log(instance.props)
  switch (index) {
    case 0:
      return (<div className={classes.stepContent}>
                <FeatureSelectionSection />
              </div>)
    case 1:
      return (<div className={classes.stepContent}>
                <LoadScriptSection />
              </div>)
    case 2:
      return (<div className={classes.stepContent}>
                <AuthenticationSection code={instance.props.match.params.code}/>
              </div>)
    case 3:
      return (<div className={classes.stepContent}>
                <FuelSection />
              </div>)
    case 4:
      return (<div className={classes.stepContent}>
                <StartComputationSection />
              </div>)
    default:
      return 'Uknown stepIndex'
  }
}

const mapStateToProps = (state) => {
  return {
    web3: state.contractReducer.web3,
    step: state.dashboardReducer.step
  }
};

const mapDispatchToProps = dispatch => ({
  initWeb3Action: (web3) => dispatch(initWeb3Action(web3))
})

DashboardPage.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(dashboardPageStyle),
  connect(mapStateToProps, mapDispatchToProps)
)(DashboardPage);
