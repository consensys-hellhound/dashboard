import React from 'react'
import compose from 'recompose/compose'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// @material-ui/core components
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import CircularProgress from '@material-ui/core/CircularProgress'

// material kit react components
import Button from "components/CustomButtons/Button"

//redux
import { connect } from 'react-redux'
import { changeDashboardTokenAction, changeDashboardStepAction  } from 'redux/actions/DashboardAction'
import { changeWalletAddressAction, 
  changeProxyAddressAction, initFueledWeb3Action  } from 'redux/actions/ContractAction'

//assets
import door from "assets/img/doar.jpg"

// view
import View from "views/common/GenericView"

//style
import authenticationStyle from "assets/jss/views/dashboardSections/stepSelectionStyle"

// ethereum Contract
import ImmersionServiceArtifacts from 'contracts/Immersion.json'
import { default as contract } from 'truffle-contract'

import Fuel from "./../../../utils/web3/fuel-web3-provider/src"
import Web3 from "web3";

let ImmersionService = contract(ImmersionServiceArtifacts)

class AuthenticationSection extends View.GenericView {
  state = {
    openAuthenticationInProgress: false,
    openDialogMessage: false,
    dialogMessage: "",
    fueledWeb3: null
  };

  componentDidMount() {

    window.setInterval(this.updateAddress.bind(this), 1000);

    this.authenticateWithOauth2()

    window.scrollTo(0, 0)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.step}>
        <center>
          <h2>Abandon all hope, you who enter</h2>
          <br/>
          <img src={door} className={classes.imageContainer}  alt="..."  />
          <br/>
          <h4>1) Enter you public address</h4>
          <TextField
            className={classes.textField}
            value={this.props.walletAddress}
            onChange={this.handleChangeAddress()}
            margin="normal"
            fullWidth
            variant="outlined"
          />
          <br/><br/>
          <h4>2) Download the light cerberus client</h4>
          <Button
                size="lg"
                target="_blank"
                rel="noopener noreferrer">Download</Button>
          <br/><br/>
          <h4>3) Open the door</h4>
          <Button
                color="info"
                size="lg"
                disabled={!this.props.walletAddress || this.props.walletAddress.trim()===""}
                href={this.props.irisAddress+"/token/oauth/gitlab"}
                rel="noopener noreferrer">Authenticate</Button>
        </center>
        { this.renderMessageDialog() }
        { this.renderProgressDialog() }
      </div>
    );
  }


  renderProgressDialog = () => {
    const { classes } = this.props;
    console.log(this.props)
    if(this.state.openAuthenticationInProgress){
      return (
        <Dialog
            open={true}
            aria-describedby="alert-dialog-description">
        <DialogContent>
          <center>
            <CircularProgress
              className={classes.progress}
              color="primary"/>
            <br/><br/>
            <DialogContentText id="alert-dialog-description">
                Authentication in progress ...
            </DialogContentText>
          </center>
        </DialogContent>
      </Dialog>)
    }
  }

  /**
  * This method allows you to start the oauth2 step
  * Step 1
  */
  authenticateWithOauth2(){
      //if oauth2's token is available in the YRL --> continue the authentication with IRIS (get public address signature)
      this.setState(state => ({
          openAuthenticationInProgress: this.props.code!==undefined,
      }));
      if(this.props.code){
        this.authenticateWithIris()
      }
  }

  /**
  * This method allows you to query the Iris server for an authentication token
  * Step 2
  */
  authenticateWithIris(){
      var immersionAddress = (this.props.immersionAddress)?this.props.immersionAddress.replace("0x",""):""
      var walletAddress = (this.props.walletAddress)?this.props.walletAddress.replace("0x",""):""
      fetch(this.props.irisAddress+"/token/authenticate/", {
        method: 'post',
        mode: 'cors',
        headers: {"Content-Type": "application/json",},
        //TODO create object for the query
        body: "{\"contractAddress\":\""+immersionAddress+"\", \"token\":\""+this.props.code +"\",  \"walletAddress\":\""+walletAddress+"\"}"
      }).then(response => {
        return response.json()
      })
      .then(body => {
        console.log("Response from backend received : ", body.tokenValue)
        this.props.changeDashboardTokenAction("0x"+body.tokenValue)
        this.authenticateWithFuel()
      });
  }

  /**
  * This method allows you to create a proxy contract with FUEL
  * Step 3
  */
  authenticateWithFuel(){
    console.log("we call autenthicate fuel")
    console.log(this.props.walletAddress)
    fetch("http://localhost:4000/createproxy", {
      method: 'post',
      mode: 'cors',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        recoveryKey: this.props.walletAddress,
        deviceKey: this.props.walletAddress
      })
    }).then(response => {
      return response.json()
    }).then(body => {
      console.log("Response from fuel received : ", body.data.identity)
      this.props.changeProxyAddressAction(body.data.identity)
      this.authenticationCompleted()
    })
  }

  /**
  * This method is called when the authentication is completed
  * Step 4
  */
  authenticationCompleted(){
    this.setState(state => ({
        openAuthenticationInProgress: false,
    }));
    this.handleOpenMessageDialog("Authentication Completed")
  }

  /**
  * This method checks whether the public key has a valid signature
  * For dev only. This method is called by a contract
  */
  checkAuthenticate(){
    if (this.props.walletAddress.trim() === "") { //is address empty
      this.handleOpenMessageDialog("The public address of your wallet is required");
    } else {
      console.log(this.props.proxyAddress)
      const web3 = new Web3("http://35.241.135.201:8545")
      const immersionContract = new  web3.eth.Contract(
        ImmersionServiceArtifacts.abi, this.props.immersionAddress)
      this.props.web3.eth.getAccounts((error, accounts) => {
        immersionContract.methods.isAuthenticatedUser(
          this.props.proxyAddress,
          this.props.tokenValue)
          .call({ from: accounts[0] })
          .then((result) => {
              console.log("check authentication result : ", result);
              this.handleOpenMessageDialog("Check authentication result "+result);
          })
        })
    }
  }

  handleChangeAddress = name => event => {
    this.props.changeWalletAddressAction(event.target.value)
  };

  /**
  * This method queries web3 to obtain the public address of the user. If it is available
  * Step 1
  */
  updateAddress(){
    if(this.props.web3){
      // Request account access if needed
      this.props.web3.eth.getAccounts((error, accounts) => {
        if(accounts && accounts.length>0){
          this.props.changeWalletAddressAction(accounts[0])
        }
      })
    }
  }

  handleCloseMessageDialog = () => {
    this.setState({ openDialogMessage: false });
    this.props.changeDashboardStepAction(this.props.step+1)
  };


}

const mapStateToProps = (state) => {
  return {
    web3: state.contractReducer.web3,
    fueledWeb3: state.contractReducer.fueledWeb3,
    step: state.dashboardReducer.step,
    walletAddress: state.contractReducer.address,
    proxyAddress: state.contractReducer.proxyAddress,
    tokenValue: state.dashboardReducer.tokenValue,
    irisAddress: state.settingsReducer.irisAddress,
    immersionAddress: state.settingsReducer.immersionAddress

  }
};

const mapDispatchToProps = dispatch => ({
  changeDashboardTokenAction: (newToken) => dispatch(changeDashboardTokenAction(newToken)),
  changeWalletAddressAction: (newAddress) => dispatch(changeWalletAddressAction(newAddress)),
  changeProxyAddressAction: (newAddress) => dispatch(changeProxyAddressAction(newAddress)),
  initFueledWeb3Action: (web3) => dispatch(initFueledWeb3Action(web3)),
  changeDashboardStepAction: (newStep) => dispatch(changeDashboardStepAction(newStep))
})


AuthenticationSection.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(authenticationStyle),
  connect(mapStateToProps, mapDispatchToProps)
)(AuthenticationSection);
