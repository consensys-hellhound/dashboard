import React from 'react'
import compose from 'recompose/compose'
import PropTypes from 'prop-types'

// @material-ui/core components
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Checkbox from '@material-ui/core/Checkbox'
import Icon from '@material-ui/core/Icon'

// material kit react components
import Button from "components/CustomButtons/Button"

//dropzone
import ReactDropzone from 'react-dropzone'

//redux
import { connect } from 'react-redux';
import { changeDashboardStepAction  } from 'redux/actions/DashboardAction'
import { changeBytecodeComputationAction  } from 'redux/actions/DashboardAction'
import { changeDashboardSecretAction  } from 'redux/actions/DashboardAction'

//assets
import sky from "assets/img/sky.jpg"

// view
import parser  from "utils/vm/Parser"
import View from "views/common/VmView"

//vm utils
import Cryptosystem from "utils/vm/cryptosystem/Cryptosystem";
import VmDefineSecret from "utils/nats/VmDefineSecret";

//style
import loadScriptSection from "assets/jss/views/dashboardSections/stepSelectionStyle"



const cryptosystemsHomomorphic = [
  new Cryptosystem('PAILLIER',"Homomorphic", "add a+b ; mul k*a (int k)", "", "","", ""),
  new Cryptosystem('UNPADDED RSA',"Homomorphic", "mul a*b", "", "", "O(10^6)", ""),
  new Cryptosystem('ELGAMAL',"Homomorphic", "", "", "", "", ""),
];

const cryptosystemsMultiPartyComputation = [
  new Cryptosystem('CRAMER DAMGARD NIELSER',"MultiPartyComputation", "", "", "","", ""),
  new Cryptosystem('OBLIVIOUS TRANSFER',"MultiPartyComputation", "", "", "", "", ""),
  new Cryptosystem('SHAMIR SECRET SHARING',"MultiPartyComputation", "", "", "", "", ""),
];

const encryptedValue = [
    "6e572b7a87d669caf6b479d29b9b95d8920c45caa2bd28943a34ab2a97a331bd",
    "48bfe32bc9b0b8c4dbec902d60a835107bfeeface538a14653c77a97e22f114f",
    "2cac8d2b51feef43e9c5fc866fe7ad7331bed19416df6886e41b161e31b69874",
    "796802af932609f63ea843cb8bf957ce5a3c05c3b099951f0e1031681c38a784",
    "71831ec81655e17bdfe3c475fdbec4747a99dd8566a905ed3f4249049a43dba5",
    "74bce0f71b971dafd2eae89d803116b38e1fbdfb36e90269ffe2bbdbfdf68868",
    "1a024ae12e7d08191a2ce5d59e5c93611e85a802b5938afe94da5ee766277ce4",
    "6f96ecbf75dd1ee5cfdbca63dce33e2acfe81465ef592105f33edc8f0586fcff",
    "31a44da2d1631337d1fb3d31750b28db4470fcd92fddc4781965922728271701",
    "7375a027fdb5760b60768fc8c25448abc173f5610b23be0c80f1b6d405b84fad"
]

var nats, sid;

class LoadScriptSection extends View.VmView {

  state = {
    cryptosystem: "PAILLIER",
    currentFileName: "", // script filename
    keysFound: new Map(), //keys found on the script (ex. [SECRET1])
    secretFromPhone:false, // secret come from phone
    listIns: [] //list instructions found on the script
  }

  componentDidMount(){
    window.scrollTo(0, 0)
    const instance = this;

    //*********** FOR DEMO ONLY ****************/

    fetch(this.props.pythiaAddress+'/cryptosystem',{
      method: 'get'
    }) //TODO use response of pythia

    //connect the nats
    console.log("connecting to the nats " + this.props.natsAddress)

    nats = require("websocket-nats").connect(this.props.natsAddress)

    sid = nats.subscribe("domain.event", function(msg) {
      console.log("Received a message: " + JSON.stringify(msg));
      var natsCommand = JSON.parse(msg)
      switch (natsCommand.type) {
        case VmDefineSecret.TYPE:
          instance.state.keysFound.set("[SECRET1_______________________________________________________]",natsCommand.secret)
          console.log("keysfound", instance.state.keysFound)
          instance.setState({ keysFound: instance.state.keysFound, secretFromPhone: true })
          break;
        default:
          console.log("Unknown message");
      }
    });

    //*********** END FOR DEMO ONLY ****************/

  }

  componentWillUnmount() {
    nats.unsubscribe(sid)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.step}>
        <center><h2>Tell us what you want to do</h2></center>
        <br/>
        <img src={sky} className={classes.imageContainer}  alt="..."  />
        <br/>
        <h4>We have selected a list of cryptosystems based on your needs</h4>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Type</TableCell>
              <TableCell>Possible Operations</TableCell>
              <TableCell>Strenghts</TableCell>
              <TableCell>Weaknesses</TableCell>
              <TableCell>Complexity</TableCell>
              <TableCell>Time</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {((this.props.cryptosystemVmConfiguration.protection==="he")?cryptosystemsHomomorphic:cryptosystemsMultiPartyComputation).map(row => {
              console.log(row)
              return (
                <TableRow key={row.uuid}>
                  <TableCell><Checkbox value={row.name} checked={this.state.cryptosystem===row.name} onChange={this.handleSelectCryptosystem.bind(this)}/></TableCell>
                  <TableCell component="th" scope="row">{row.name}</TableCell>
                  <TableCell>{row.type}</TableCell>
                  <TableCell>{row.possibleOperations}</TableCell>
                  <TableCell>{row.strenghts}</TableCell>
                  <TableCell>{row.weaknesses}</TableCell>
                  <TableCell>{row.complexity}</TableCell>
                  <TableCell>{row.time}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        <br/>
        <br/>
        <h4>Load your algorithm based on the {this.state.cryptosystem} cryptosystem</h4>
        <br/>
        {this.renderDropzone()}
        <br/>
        {this.renderDynamicForm()}
        <br/>
        {this.renderBytecode()}
        <br/>
        <Button
              target="_blank"
              onClick={this.handleReset.bind(this)}
              rel="noopener noreferrer">Back</Button>
        <Button
              color="info"
              target="_blank"
              disabled={!this.state.listIns}
              onClick={this.handleNextStep.bind(this)}
              rel="noopener noreferrer">Confirm</Button>
      </div>
    )
  }

  renderDynamicForm(){
    const { classes } = this.props;
    var views = []
    if(!this.state.secretFromPhone){
      for (const [key, value] of this.state.keysFound) {
        views.push(
          <div name={key}>
            <TextField
                className={classes.textField}
                label={key.replace(/_/g,"")}
                value={value}
                onChange={this.handleKeyChange(key)}
                margin="normal"
                type="number"
                fullWidth
                inputProps={{ min: "0", max: "10", step: "1" }}
                variant="outlined" />
          </div>
        )
      }
    } else {
        views.push(<h2>Encrypted secret received from a phone</h2>)
    }
    return views
  }

  renderDropzone(){
    const { classes } = this.props;
    return (
      <ReactDropzone className={classes.dropzone} accept=".hh" onDrop={this.handleFileSelected.bind(this)}>
        <center>
        <h6>Drop your script here (.hh)</h6>
        <Icon fontSize="large" color="disabled" >cloud_upload</Icon>
        <p>{this.state.currentFileName!==""?"Current uploaded file : "+this.state.currentFileName:""}</p>
        </center>
      </ReactDropzone>
    )
  }

  /**
  *  This method allows to choose a specific cryptosystem
  *
  */
  handleSelectCryptosystem = event => {
    if (event.target.checked) {
      this.setState({ cryptosystem: event.target.value })
    }else{
      this.setState({ cryptosystem: undefined })
    }
  }

  /**
  *  This method allows to change a specific key found on the script
  *
  */
  handleKeyChange = name => event => {
    this.state.keysFound.set(name, event.target.value)
    this.setState({ keysFound: this.state.keysFound })
  }

  handleFileSelected(acceptedFiles) {
    acceptedFiles.forEach(file => {
        const reader = new FileReader();
        reader.onload = () => {
            const fileAsBinaryString = reader.result
            //search key on the script
            var keysFound = fileAsBinaryString.match(/\[[^[.]*\]/g);

            this.setState({
              keysFound: (keysFound)?new Map(keysFound.map(key=>[key, 0])):new Map(),
              currentFileName: file.name,
              listIns: parser(fileAsBinaryString)
            })
            this.props.changeBytecodeComputationAction(fileAsBinaryString)
            console.log("File read :", fileAsBinaryString)
        };
        reader.onabort = () => console.log('file reading was aborted')
        reader.onerror = () => console.log('file reading has failed')

        reader.readAsBinaryString(file);
    })
  }

  /**
  *  This method allows to go to the first step of the dashboard
  *
  */
  handleReset = () => {
    this.props.changeDashboardStepAction(0)
  };

  /**
  *  This method allows to go to the next step
  *
  */
  handleNextStep = () => {

    this.prepareBytecode()

    if(this.props.tokenValue===""){
      //of token not available --> go to the authentication
      this.props.changeDashboardStepAction(this.props.step+1)
    }else{
      //if token already available --> skip authentication
      this.props.changeDashboardStepAction(this.props.step+3)
    }
  };

  /**
  * This method allows to prepare bytecode (replace keys if present and store it)
  *
  */
  prepareBytecode(){
    var preparedBytecode = this.props.bytecode
    for (const [key, value] of this.state.keysFound) {
        preparedBytecode = preparedBytecode.replace(key,encryptedValue[value])
        this.props.changeDashboardSecretAction((parseInt(value,10)+2+1+4)*3) // //*********** FOR DEMO ONLY ****************/
    }

    console.log("prepared bytecode : ", preparedBytecode)
    this.props.changeBytecodeComputationAction(preparedBytecode)
  }

}


const mapStateToProps = (state) => {
  return {
    pythiaAddress: state.settingsReducer.pythiaAddress,
    natsAddress: state.settingsReducer.natsAddress,
    step: state.dashboardReducer.step,
    tokenValue: state.dashboardReducer.tokenValue,
    bytecode: state.dashboardReducer.bytecode,
    cryptosystemVmConfiguration: state.dashboardReducer.cryptosystemVmConfiguration
  }
};

const mapDispatchToProps = dispatch => ({
  changeDashboardStepAction: (newStep) => dispatch(changeDashboardStepAction(newStep)),
  changeBytecodeComputationAction: (newBytecode) => dispatch(changeBytecodeComputationAction(newBytecode)),
  changeDashboardSecretAction: (newSecret) => dispatch(changeDashboardSecretAction(newSecret)),
})

LoadScriptSection.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(loadScriptSection),
  connect(mapStateToProps, mapDispatchToProps)
)(LoadScriptSection);
