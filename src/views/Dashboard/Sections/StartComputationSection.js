import React from "react";
import compose from "recompose/compose";
import PropTypes from "prop-types";

// @material-ui/core components
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";

// material kit react components
import Button from "components/CustomButtons/Button";

import PieChart from "react-simple-pie-chart";
import CircleLoader from "react-spinners/CircleLoader";

//redux
import { connect } from "react-redux";
import { changeDashboardStepAction } from "redux/actions/DashboardAction";

// view
import parser  from "utils/vm/Parser"
import View from "views/common/VmView";

//assets
import monster from "assets/img/monster.jpg";

//style
import startComputationSection from "assets/jss/views/dashboardSections/stepSelectionStyle";

//broker
import VmExecutionCommand from "utils/nats/VmExecutionRequest";
import VmExecutionResult from "utils/nats/VmExecutionResult";
import VmExecutionStep from "utils/nats/VmExecutionStep";
import HowlVmExecutionCommand from "utils/nats/HowlVmExecutionCommand";

var indexInstruction = new Map()
var nats, sid;

class StartComputationSection extends View.VmView {
  state = {
    result: [],
    wantedNodes: 1,
    listIns: []
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    const instance = this;

    //connect the nats
    console.log("connecting to the nats " + this.props.natsAddress)

    nats = require("websocket-nats").connect(this.props.natsAddress)
    //subscribe to the nats for the vm execution result
    sid = nats.subscribe("domain.event", function(msg) {
      console.log("Received a message: " + JSON.stringify(msg));
      var natsCommand = JSON.parse(msg)
      switch (natsCommand.type) {
        case HowlVmExecutionCommand.TYPE:
          var newResult = instance.state.result;
          natsCommand.nodes.forEach(function(nodeId) {
            indexInstruction.set(nodeId, -1)
            newResult = newResult.concat(
              new VmExecutionResult(
                natsCommand.computationId,
                nodeId,
                natsCommand.output,
                VmExecutionResult.STATUS_IN_PROGRESS
              )
            );
          })
          instance.setState({ result: newResult , listIns: parser(instance.props.bytecode)});
          break;
        case VmExecutionResult.TYPE:
          setTimeout(function() {
            //Start the timer
            instance.state.result.forEach(function(node) {
              if (node.nodeId === natsCommand.nodeId) {
                var ins = instance.state.listIns[indexInstruction.get(node.nodeId)]
                if(ins){ins.nodes.remove(node.nodeId)}
                node.output = natsCommand.output;
                node.status = VmExecutionResult.STATUS_SUCCESS;
              }
            })
            instance.setState({ result: instance.state.result })
          }, Math.random() * (10000 - 8000) + 8000)
          break;
        case VmExecutionStep.TYPE:
          setTimeout(function() {
            console.log("Total ",instance.state.listIns)
            var nodeId = natsCommand.nodeId
            var currentIndex = indexInstruction.get(nodeId);
            if(currentIndex>=0 && instance.state.listIns[currentIndex]){
              instance.state.listIns[currentIndex].nodes.remove(nodeId)
            }
            var newIndex = indexInstruction.get(nodeId)+1;
            if(newIndex<instance.state.listIns.length && instance.state.listIns[newIndex]){
              indexInstruction.set(nodeId,newIndex)
              instance.state.listIns[newIndex].nodes.push(nodeId)
            }
            instance.setState({ listIns: instance.state.listIns })
          }, Math.random() * (7000 - 4000) + 4000)
          break;

        default:
          console.log("Unknown message");
      }
    });
  }

  componentWillUnmount() {
    nats.unsubscribe(sid)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.step}>
        <center>
          <h2>Computation</h2>
          <br />
          <img src={monster} className={classes.imageContainer} alt="..." />
          <br />
          {this.renderWantedNodes()}
          <br />
          {this.renderNodeResult()}
          <br />
          <br />
        </center>
        {this.renderBytecode()}
        <center>
          {this.renderChart()}
          <br />
          <Button
            target="_blank"
            onClick={this.handleReset.bind(this)}
            rel="noopener noreferrer">Reset</Button>
          <Button
            color="info"
            target="_blank"
            onClick={this.handleStartComputation.bind(this)}
            rel="noopener noreferrer">Start computation</Button>
        </center>
      </div>
    );
  }

  renderWantedNodes() {
    const { classes } = this.props;

    return (
      <TextField
        label="Wanted nodes"
        value={this.state.wantedNodes}
        onChange={this.handleChange("wantedNodes")}
        type="number"
        className={classes.textField}
        fullWidth
        margin="normal"
        inputProps={{ min: "0", max: "10", step: "1" }}
      />
    );
  }

  renderNodeResult() {
    const { classes } = this.props;
    if (this.state.result.length > 0) {
      return (
        <div>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Node ID</TableCell>
                <TableCell>Output</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.result.map(row => {
                return (
                  <TableRow key={row.nodeId}>
                    <TableCell>
                      <CircleLoader
                        color={"#00ACC1"}
                        loading={
                          row.status === VmExecutionResult.STATUS_IN_PROGRESS
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <b>{row.nodeId}</b>
                    </TableCell>
                    <TableCell>
                      <div className={classes.textMultiline}>
                        {JSON.stringify(row.output)}
                      </div>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
      );
    }
  }

  renderChart() {
    var isCompleted = this.state.result.length > 0;
    if (isCompleted) {
      this.state.result.forEach(function(node) {
        if (node.status === VmExecutionResult.STATUS_IN_PROGRESS) {
          isCompleted = false;
        }
      });
    }
    if (isCompleted) {
      const { classes } = this.props;
      var finalResult = Object.values(groupBy(this.state.result));
      var validNodesNumber = arrayMax(finalResult).length;
      return (
        <div className={classes.pieChart}>
          <PieChart
            slices={[
              {
                color: "#00ACC1",
                value: (validNodesNumber / this.state.result.length) * 100
              },
              {
                color: "#E0115F",
                value:
                  ((this.state.result.length - validNodesNumber) /
                    this.state.result.length) *
                  100
              }
            ]}
          />
          <br />
          <h2>RESULT = {this.props.secret}</h2>
          <b>
            {((validNodesNumber / this.state.result.length) * 100).toFixed(2)} percent of the
            results you obtained are valid
          </b>
        </div>
      );
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  /**
   *  This method allows to go to the first step of the dashboard
   *
   */
  handleReset = () => {
    this.props.changeDashboardStepAction(0);
  };

  /**
   *  This method allows to start the computation (publish event in nats)
   *
   */
  handleStartComputation = () => {
    // reset list node
    this.setState({ result: [] });
    // push nats event
    var vmExecutionEvent = new VmExecutionCommand(
      this.props.bytecode,
      parseInt(this.state.wantedNodes, 10)
    );
    console.log(JSON.stringify(vmExecutionEvent));
    nats.publish("domain.event", JSON.stringify(vmExecutionEvent));
  };
}


//TODO create library
var groupBy = function(xs) {
  return xs.reduce(function(rv, x) {
    (rv[JSON.stringify(x.output)] = rv[JSON.stringify(x.output)] || []).push(x);
    return rv;
  }, {});
};

//TODO create library
function arrayMax(arr) {
  return arr.reduce(function(p, v) {
    return p.length > v.length ? p : v;
  });
}

/*eslint no-extend-native: ["error", { "exceptions": ["Array"] }]*/
Array.prototype.remove = function(elem, all) {
  for (var i=this.length-1; i>=0; i--) {
    if (this[i] === elem) {
        this.splice(i, 1);
        if(!all)
          break;
    }
  }
  return this;
};

const mapStateToProps = state => {
  return {
    natsAddress: state.settingsReducer.natsAddress,
    bytecode: state.dashboardReducer.bytecode,
    secret: state.dashboardReducer.secret,
  };
};

const mapDispatchToProps = dispatch => ({
  changeDashboardStepAction: newStep =>
    dispatch(changeDashboardStepAction(newStep))
});

StartComputationSection.propTypes = {
  classes: PropTypes.object
};

export default compose(
  withStyles(startComputationSection),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(StartComputationSection);
