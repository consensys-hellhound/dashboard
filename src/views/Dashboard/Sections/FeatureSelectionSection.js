import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';

// @material-ui/core components
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

// material kit react components
import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";

//redux
import { connect } from 'react-redux';
import { changeDashboardStepAction  } from 'redux/actions/DashboardAction'
import { changeDashboardCryptosystemVmConfigurationAction  } from 'redux/actions/DashboardAction'

//vm utils
import VmConfiguration from "utils/vm/cryptosystem/VmConfiguration";

//assets
import road from "assets/img/road.jpg";

//style
import featureSelectionStyle from "assets/jss/views/dashboardSections/stepSelectionStyle";

class FeatureSelectionSection extends React.Component {
  state = {
    activeStep: 0,
    usecase: "compute", //TODO remove and use redux VmConfiguration Object
    mode: "interactive", //TODO remove and use redux VmConfiguration Object
    interactionLevel: "dontcare", //TODO remove and use redux VmConfiguration Object
    timeConstraint: "dontcare", //TODO remove and use redux VmConfiguration Object
    protection: "he" //TODO remove and use redux VmConfiguration Object
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={[classes.step, "step"].join(" ")}>
        <center><h2>Choose your configuration</h2></center>
        <br/>
        <img src={road} className={classes.imageContainer}  alt="..."  />
        <center><h3>There is <b>3</b> available cryptosystems</h3></center>
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => {
            return (
              <Step key={label}>
              <StepLabel className={classes.iconContainer}> {label}</StepLabel>
                <StepContent>
                  {getStepContent(index, this)}
                  <div className={classes.actionsContainer}>
                    <div>
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.button}>
                        Back
                      </Button>
                      <Button
                        variant="contained"
                        onClick={activeStep === steps.length - 1 ? this.handleSearch : this.handleNext}
                        className={[classes.button, classes.buttonNext].join(" ")}>
                        {activeStep === steps.length - 1 ? 'Search' : 'Next'}
                      </Button>
                    </div>
                  </div>
                </StepContent>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&quot;re finished</Typography>
            <Button onClick={this.handleReset} className={classes.button}>
              Reset
            </Button>
          </Paper>
        )}
      </div>
    );
  }

  /**
  *  This method allows you to go to a next step during the choice of features
  *
  */
  handleNext = () => {
    this.setState(state => ({
        activeStep: state.activeStep + 1,
    }))
  }

  /**
  *  This method allows to launch the search on pythia
  *
  */
  handleSearch = () => {
    this.props.changeDashboardCryptosystemVmConfigurationAction(
      new VmConfiguration(
        this.state.usecase,
        this.state.mode,
        this.state.interactionLevel,
        this.state.timeConstraint,
        this.state.protection
      )
    )
    this.props.changeDashboardStepAction(this.props.step+1)
  }

  /**
  * This method allows to go back during the choice of features
  *
  */
  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }))
  }

  /**
  * This method resets the feature selection form
  *
  */
  handleReset = () => {
    this.setState({
      activeStep: 0,
    })
  }

  handleChangeToggle = name => event => {
    this.setState({ [name]: event.target.checked });
  }

  handleChangeRadio = name => event => {
    this.setState({ [name]: event.target.value });
  }
}


/**
* The steps of the feature selection form
*
*/
function getSteps() {
  return ['What is your use case ?', 'How do you want to communicate with the network ?', 'How do you want to protect your privacy ?', 'Do you have time constraints ?'];
}

/**
* Step content of the feature selection form
*
*/
function getStepContent(step, instance) {
  const { classes } = instance.props;
  switch (step) {
    case 0:
      return (
        <div className={classes.stepContent}>
          <h5>Choose what you want to do with Hellhound</h5>
          <RadioGroup
            aria-label="UseCase"
            name="usecase"
            className={classes.group}
            value={instance.state.usecase}
            onChange={instance.handleChangeRadio("usecase")} >
            <FormControlLabel value="compute" control={<Radio />} label="Compute" />
            <FormControlLabel value="verify" disabled control={<Radio />}  label="Search"/>
            <FormControlLabel value="search" disabled control={<Radio />}  label="Verify"/>
          </RadioGroup>
        </div>
      )
    case 1:
      return (
        <div className={classes.stepContent}>
          <h5>You can choose between interactive mode and non-interactive mode</h5>
          <h5>If you choose interactive mode you must <b>stay online</b> and <b>participate in the computation</b></h5>
          <h5>This choice will result in <b>more complex communication</b> and <b>less computation for the server</b></h5>
          <RadioGroup
            aria-label="mode"
            name="mode"
            className={classes.group}
            value={instance.state.mode}
            onChange={instance.handleChangeRadio("mode")} >
            <FormControlLabel value="interactive" control={<Radio />} label="Interactive" />
            <FormControlLabel value="nonInteractive" control={<Radio />}  label="Non Interactive"/>
          </RadioGroup>
          <h5>In case of interactive mode you can choose your <b>level of interaction</b></h5>
          <h5>If you choose minimal, you wil get a hybrid approach (which is a mix of Homomorphic encryption and Multiparty computation)</h5>
          <RadioGroup
            aria-label="interactionLevel"
            name="interactionLevel"
            className={classes.group}
            value={instance.state.interactionLevel}
            onChange={instance.handleChangeRadio("interactionLevel")} >
              <FormControlLabel value="minimal" disabled={instance.state.mode==="interactive"?false:true}  control={<Radio />} label="Minimal" />
              <FormControlLabel value="maximal" disabled={instance.state.mode==="interactive"?false:true} control={<Radio />}  label="Maximal"/>
              <FormControlLabel value="dontcare" disabled={instance.state.mode==="interactive"?false:true} control={<Radio />}  label="I don't care"/>
          </RadioGroup>
          <br/>
          {(instance.state.interactionLevel==="minimal") &&
            <SnackbarContent message={ <span> Careful what you wish for, by choosing this, you will have more computation complexity !</span> } color="warning"/>}
        </div>
      )
    case 2:
      return (
        <div className={classes.stepContent}>
          <h5>Hellhound is concerned about your privacy. Hellhound make your data confidential by protecting them. So no one will be able to recover your secrets</h5>
          <h5>You can choose between Homomorphic encryption and Secure Multiparty Computation</h5>
          <RadioGroup
            aria-label="protection"
            name="protection"
            className={classes.group}
            value={instance.state.protection}
            onChange={instance.handleChangeRadio("protection")} >
              <FormControlLabel value="he" control={<Radio />} label="Homomorphic encryption" />
              <FormControlLabel value="smcp" control={<Radio />}  label="Secure Multiparty Computation"/>
          </RadioGroup>
        </div>
      )
    case 3:
      return (
        <div className={classes.stepContent}>
          <h5>Depending on your time constraints, Hellhound will find the configuration that suits you best</h5>
          <RadioGroup
            aria-label="TimeConstraint"
            name="timeConstraint"
            className={classes.group}
            value={instance.state.timeConstraint}
            onChange={instance.handleChangeRadio("timeConstraint")} >
            <FormControlLabel value="fast" control={<Radio />} label="Fast" />
            <FormControlLabel value="medium" control={<Radio />}  label="Medium"/>
            <FormControlLabel value="dontcare" control={<Radio />}  label="I don't care"/>
          </RadioGroup>
        </div>
      )
    default:
      return (
        <h5>Not found</h5>
      )

  }
}

const mapStateToProps = (state) => {
  return {
    step: state.dashboardReducer.step,
    cryptosystemVmConfiguration: state.dashboardReducer.cryptosystemVmConfiguration
  }
};

const mapDispatchToProps = dispatch => ({
  changeDashboardStepAction: (newStep) => dispatch(changeDashboardStepAction(newStep)),
  changeDashboardCryptosystemVmConfigurationAction: (newTYpe) => dispatch(changeDashboardCryptosystemVmConfigurationAction(newTYpe))
})

FeatureSelectionSection.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(featureSelectionStyle),
  connect(mapStateToProps, mapDispatchToProps)
)(FeatureSelectionSection);
