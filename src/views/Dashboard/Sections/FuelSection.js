import React from 'react';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';

// @material-ui/core components
import { withStyles } from '@material-ui/core/styles';

// material kit react components
import Button from "components/CustomButtons/Button";

//redux
import { connect } from 'react-redux';
import { changeDashboardStepAction  } from 'redux/actions/DashboardAction'

//style
import fuelStyle from "assets/jss/views/dashboardSections/stepSelectionStyle";

import { CardElement, injectStripe, PaymentRequestButtonElement, StripeProvider } from 'react-stripe-elements';

import RetentionServiceArtifcat from "contracts/Retention.json"


import Fuel from "./../../../utils/web3/fuel-web3-provider/src"
import Web3 from "web3";


const stripeKey = 'pk_test_uIwCkJRngOpROAMK5PDPXvzZ';


class FuelSection extends React.Component {
  constructor (props) {
    super(props);
 
    this.state = {
      retentionInstance: null
    };
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.step}>
        <center> <h1>Stake ether into hellhound contract</h1> </center>

        {/* <StripeProvider apiKey={stripeKey}>
          <PaymentRequestButtonElement
            paymentRequest={this.state.paymentRequest}
            className="PaymentRequestButton"
            style={{
              // For more details on how to style the Payment Request Button, see:
              // https://stripe.com/docs/elements/payment-request-button#styling-the-element
              paymentRequestButton: {
                theme: 'light',
                height: '64px',
              },
            }}
          />
        </StripeProvider> */}
        <Button onClick={this.handleStake.bind(this)} >Pay</Button>
        

        <Button
          color="info"
          size="lg"
          target="_blank"
          onClick={this.handleNextStep.bind(this)}
          rel="noopener noreferrer">
          Next Step
        </Button>
      </div>
    );
  }

  /**
  *  This method allows to launch the search on pythia
  *
  */
  handleNextStep = () => {
    this.props.changeDashboardStepAction(this.props.step +1)
  };

  handleStake = () => {
    console.log('call handle service')
    console.log('proxy identity', this.props.proxyAddress)
    const fuel = new Fuel({
      web3Provider: window.web3,
      proxyAddress: "0x397C37ead490923688652D6fACae2Cc327B680f0", //this.props.proxyAddress,
      rpcUrl: "http://35.241.135.201:8545",
      fuelUrl: 'http://localhost:4000/relay',
      txRelayAddress: '0x8d78b24fb10af5f67488d912e6da5f0ff0b066ef',
      metaIdentitymanagerAddress: '0xb29ee73adc6179865684cce048ebc640d2a91910',
      txSenderAddress: '0xfd12370ac2210964b92695f08ad7a84516c77eeb',
      whiteListAddress: '0x0000000000000000000000000000000000000000'
    })
    const web3Fuel = new Web3(fuel.start())
    console.log(web3Fuel.version)
    console.log(this.props.tokenValue)

    web3Fuel.eth.getBalance("0xfd12370ac2210964b92695f08ad7a84516c77eeb", (error, result) => {
      console.log(error)
      console.log(result)
    })
    const retentionContract = new web3Fuel.eth.Contract(
      RetentionServiceArtifcat.abi,
      this.props.retentionAddress)
      retentionContract.methods.stake(this.props.tokenValue)
      .send({ from: "0x7eA59207d90d6fc10A4EfF5A48D6Ec89Ce455658" })
      .then(result => {
        console.log(result)
      }).catch(error => {
        console.log(error)
      })
      // retentionContract.methods.stake(this.props.tokenValue, (error, result) => {
      //   console.log(error)
      //   console.log(result)
      // })
      // retentionContract.methods.stake(this.props.tokenValue)
      // .send({ from: window.web3.eth.defaultAccount })
      // .on('error', (error) => {
      //   console.log(error)
      // })
      // .on('transactionHash', (transactionHash) => {
      //   console.log('This the transactionHash', transactionHash)
      // })
  }

}

const mapStateToProps = (state) => {
  return {
    step: state.dashboardReducer.step,
    retentionAddress: state.settingsReducer.retentionAddress,
    proxyAddress: state.contractReducer.proxyAddress,
    fueledWeb3: state.contractReducer.fueledWeb3,
    tokenValue: state.dashboardReducer.tokenValue
  }
};

const mapDispatchToProps = dispatch => ({
  changeDashboardStepAction: (newStep) => dispatch(changeDashboardStepAction(newStep))
})

FuelSection.propTypes = {
  classes: PropTypes.object,
};

export default compose(
  withStyles(fuelStyle),
   connect(mapStateToProps, mapDispatchToProps)
)(FuelSection);
