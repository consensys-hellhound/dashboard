import React from "react";


// view
import View from "views/common/GenericView";

import Chip from '@material-ui/core/Chip';

//vm utils
import LoadKey from "utils/vm/bytecode/loadKey/LoadKey";
import LoadRegistry from "utils/vm/bytecode/loadRegistry/LoadRegistry";
import PaillierAddConstant from "utils/vm/bytecode/paillierAddConstant/PaillierAddConstant";
import PaillierMulConstant from "utils/vm/bytecode/paillierAddConstant/PaillierMulConstant";
import PaillierAddCiphers from "utils/vm/bytecode/paillierAddConstant/PaillierAddCiphers";


class VmView extends  View.GenericView {

  renderBytecode = () => {
    return (
      displayBytecode(this)
    )
  }

}


function displayBytecode(instance){
  const { classes } = instance.props;
  var view = []
  instance.state.listIns.forEach(function(ins) {
    switch (ins.code) {
      case LoadKey.CODE:
        view.push(<div><Chip className={classes.chip} variant="outlined" label={ins.code+" "+ins.name}/>{displayChips(instance, ins.nodes)}{displayLoadKey(ins)}</div>)
        break;
      case LoadRegistry.CODE:
        view.push(<div><Chip className={classes.chip} variant="outlined" label={ins.code+" "+ins.name}/>{displayChips(instance, ins.nodes)}{displayLoadRegistry(ins)}</div>)
        break;
      case PaillierAddConstant.CODE:
      case PaillierMulConstant.CODE:
      case PaillierAddCiphers.CODE:
        view.push(<div><Chip className={classes.chip} variant="outlined" label={ins.code+" "+ins.name}/>{displayChips(instance, ins.nodes)}{displayPaillierOperation(ins)}</div>)
        break;
      default:
        view.push(<div><b>{ins.code}</b> Unknown ins</div>)
    }
  });
  return view
}

function displayLoadKey(loadKeyIns){
  return (
    <div>
      {addTab(1)}<b>{loadKeyIns.slot}</b> Slot
      <br/>
      {addTab(1)}<b>{loadKeyIns.keyType}</b> Key Type
      <br/>
      {addTab(1)}<b>{loadKeyIns.keyUsage}</b> Key Usage
      <br/>
      {addTab(1)}<b>{loadKeyIns.keySize}</b> Key Size
      <br/>
      {displayKeyParams(loadKeyIns.keys)}
    </div>)
}

function displayKeyParams(keys){
  var view = []
  keys.forEach(function(key) {
    view.push(<div>{addTab(2)}<b>{key.tag}</b> <b>{key.length}</b> <b>{key.value}</b> {key.getName()}<br/></div>)
  });
  return view
}

function displayLoadRegistry(loadRegistryIns){
  return (
    <div>
      {addTab(1)}<b>{loadRegistryIns.slot}</b> Slot
      <br/>
      {addTab(1)}<b>{loadRegistryIns.entrySize}</b> Entry size
      <br/>
      {addTab(2)}<b>{loadRegistryIns.value}</b> Value
    </div>)
}

function displayPaillierOperation(paillierOperationIns){
  return (
    <div>
      {addTab(1)}<b>{paillierOperationIns.slot}</b> Slot
      <br/>
      {addTab(1)}<b>{paillierOperationIns.firstOperandRegistrySlot}</b> First operand Registry Slot
      <br/>
      {addTab(1)}<b>{paillierOperationIns.secondOperandRegistrySlot}</b> Second operand Registry Slot
      <br/>
      {addTab(1)}<b>{paillierOperationIns.output}</b> Output
    </div>)
}

function displayChips(instance, chipsContent){
  const { classes } = instance.props;
  var view = []
  chipsContent.forEach(function(content) {
    view.push(<span><Chip label={content.substring(content.length-4, content.length)} className={classes.chip}/></span>)
  });
  return view
}

function addTab(size){
  var view = []
  for (var i = 0 ; i < size ; i++){
    view.push(<span key={i}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>)
  }
  return view;
}


export default { VmView };
