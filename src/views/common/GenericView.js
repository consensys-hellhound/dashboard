import React from "react";


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import Button from '@material-ui/core/Button';

import Particles from 'react-particles-js';

class GenericView extends React.Component {

  renderParticle = () => {
    if(!isMobile.any()){
      return <Particles
          style={{width:"100%",top:"0", position:"absolute"}}
          params={{
            "particles": {
                "number": {
                    "value": 50
                },
            },
            "interactivity": {
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "repulse"
                    }
                }
            }
        }} />
    }
  }

  renderMessageDialog = () => {
    return (
      <Dialog
          open={this.state.openDialogMessage}
          onClose={this.handleCloseMessageDialog}
          aria-describedby="alert-dialog-description"
        >
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {this.state.dialogMessage}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={this.handleCloseMessageDialog} color="primary" autoFocus>
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  )}


  handleOpenMessageDialog = (message) => {
    this.setState({
      openDialogMessage: true,
      dialogMessage:message
    });
  };

  handleCloseMessageDialog = () => {
    this.setState({ openDialogMessage: false });
  };

}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}


export default { GenericView };
