export default (state = {
  step: 0,
  tokenValue: "",
  bytecode: "",
}, action) => {
 switch (action.type) {
  case 'CHANGE_DASHBOARD_STEP':
   return {
      step: action.step,
      tokenValue: state.tokenValue,
      bytecode: state.bytecode,
      cryptosystemVmConfiguration: state.cryptosystemVmConfiguration,
      secret: state.secret
   }
  case 'CHANGE_DASHBOARD_TOKEN':
    return {
       step: state.step,
       tokenValue: action.tokenValue,
       bytecode: state.bytecode,
       cryptosystemVmConfiguration: state.cryptosystemVmConfiguration,
       secret: state.secret
    }
  case 'CHANGE_DASHBOARD_BYTECODE_COMPUTATION':
    return {
       step: state.step,
       tokenValue: state.tokenValue,
       bytecode: action.bytecode,
       cryptosystemVmConfiguration: state.cryptosystemVmConfiguration,
       secret: state.secret
    }
  case 'CHANGE_DASHBOARD_CRYPTOSYSTEM_CONFIGURATION':
    return {
       step: state.step,
       tokenValue: state.tokenValue,
       bytecode: state.bytecode,
       cryptosystemVmConfiguration: action.cryptosystemVmConfiguration,
       secret: state.secret
    }
  case 'CHANGE_DASHBOARD_SECRET':
    return {
       step: state.step,
       tokenValue: state.tokenValue,
       bytecode: state.bytecode,
       cryptosystemVmConfiguration: state.cryptosystemVmConfiguration,
       secret : action.secret
    }
  default:
   return state
 }
}
