export default (state = {
  irisAddress: "http://35.233.95.141",
  pythiaAddress: "http://130.211.62.11",
  immersionAddress: "0x53fec8f36050f5854b4e966341f82c3fd68841a5",
  retentionAddress: "0x4646dc5904b556bcb2148c23452990d095c77efa",
  natsAddress: "ws://104.199.10.125:4223"
}, action) => {
 switch (action.type) {
  case 'CHANGE_IRIS_ADDRESS':
    return {
      irisAddress: action.address,
      pythiaAddress: state.pythiaAddress,
      immersionAddress: state.immersionAddress,
      retentionAddress: state.retentionAddress,
      natsAddress: state.natsAddress
    }
  case 'CHANGE_IMMERSION_ADDRESS':
    return {
       irisAddress: state.irisAddress,
       pythiaAddress: state.pythiaAddress,
       immersionAddress: action.address,
       retentionAddress: state.retentionAddress,
       natsAddress: state.natsAddress
    }
  case 'CHANGE_NATS_ADDRESS':
    return {
       irisAddress: state.irisAddress,
       pythiaAddress: state.pythiaAddress,
       immersionAddress: state.immersionAddress,
       retentionAddress: state.retentionAddress,
       natsAddress: action.address
    }
  case 'CHANGE_RETENTION_ADDRESS':
    return {
       irisAddress: state.irisAddress,
       immersionAddress: state.immersionAddress,
       retentionAddress: action.address,
       natsAddress: state.address
    }
  case 'CHANGE_PYTHIA_ADDRESS':
    return {
       irisAddress: state.irisAddress,
       pythiaAddress: action.address,
       immersionAddress: state.immersionAddress,
       natsAddress: state.natsAddress
      }
  default:
    return state
 }
}
