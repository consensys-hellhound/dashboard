import { combineReducers } from 'redux';
import dashboardReducer from './DashboardReducer';
import contractReducer from './ContractReducer';
import settingsReducer from './SettingsReducer';

export default combineReducers({
  dashboardReducer, contractReducer, settingsReducer
});
