export default (state = {
  web3: null,
  fueledWeb3: null,
  address: "",
  proxyAddress: ""
}, action) => {
 switch (action.type) {
  case 'INIT_WEB3':
   return {
      web3: action.payload,
      fueledWeb3: state.fueledWeb3,
      address: state.address,
      proxyAddress: state.proxyAddress
   }
  case 'INIT_FUELED_WEB3':
   return {
      web3: state.web3,
      fueledWeb3: action.payload,
      address: state.address,
      proxyAddress: state.proxyAddress
  }
  case 'CHANGE_WALLET_ADDRESS':
   return {
     web3: state.web3,
     fueledWeb3: state.fueledWeb3,
     address: action.address,
     proxyAddress: state.proxyAddress
  }
  case 'CHANGE_PROXY_ADDRESS':
    return {
      web3: state.web3,
      fueledWeb3: state.fueledWeb3,
      address: state.address,
      proxyAddress: action.address
    }
  default:
   return state
 }
}
