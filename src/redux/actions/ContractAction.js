export const initWeb3Action = (web3) => dispatch => {
    dispatch({
        type: 'INIT_WEB3',
        payload: web3
    })
}
export const initFueledWeb3Action = (web3) => dispatch => {
    dispatch({
        type: 'INIT_FUELED_WEB3',
        payload: web3
    })
}
export const changeWalletAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: 'CHANGE_WALLET_ADDRESS',
        address: newAddress
    })
}
export const changeProxyAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: "CHANGE_PROXY_ADDRESS",
        address: newAddress
    })
}