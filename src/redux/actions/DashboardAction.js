export const changeDashboardStepAction = (newStep) => dispatch => {
 dispatch({
  type: 'CHANGE_DASHBOARD_STEP',
  step: newStep
 })
}
export const changeBytecodeComputationAction = (newBytecode) => dispatch => {
 dispatch({
  type: 'CHANGE_DASHBOARD_BYTECODE_COMPUTATION',
  bytecode: newBytecode
 })
}
export const changeDashboardTokenAction = (newToken) => dispatch => {
 dispatch({
  type: 'CHANGE_DASHBOARD_TOKEN',
  tokenValue: newToken
 })
}
export const changeDashboardCryptosystemVmConfigurationAction = (newConf) => dispatch => {
 dispatch({
  type: 'CHANGE_DASHBOARD_CRYPTOSYSTEM_CONFIGURATION',
  cryptosystemVmConfiguration: newConf
 })
}
export const changeDashboardSecretAction = (newSecret) => dispatch => {
 dispatch({
  type: 'CHANGE_DASHBOARD_SECRET',
  secret: newSecret
 })
}
