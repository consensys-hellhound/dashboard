export const changeImmersionAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: 'CHANGE_IMMERSION_ADDRESS',
        address: newAddress
    })
}

export const changeIrisAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: 'CHANGE_IRIS_ADDRESS',
        address: newAddress
    })
}
export const changePythiaAddressAction = (newAddress) => dispatch => {
 dispatch({
  type: 'CHANGE_PYTHIA_ADDRESS',
  address: newAddress
 })
}
export const changeNatsAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: 'CHANGE_NATS_ADDRESS',
        address: newAddress
    })
}

export const changeRetentionAddressAction = (newAddress) => dispatch => {
    dispatch({
        type: 'CHANGE_RETENTION_ADDRESS',
        address: newAddress
    })
}