const pathStyle = {
  stepContent: {
    paddingLeft: "30px",
    paddingBottom: "30px"
  },
  buttonNext: {
    marginTop: "7px",
    marginBottom: "3px",
    backgroundColor: "rgb(0, 172, 193) !important",
    color: '#ffffff !important'
  },
  imageContainer: {
    display: "block",
    paddingTop: "10px",
    paddingBottom: "10px",
    marginLeft: "auto",
    marginRight: "auto",
    width: "30%",
    height: "auto"
  },
  margin5: {
    margin: "5px"
  },
  step: {
     textAlign:"left",
     padding: "0px 0px 60px 0px"
  },
  textField: {
    maxWidth: "50%"
  },
  dropzone: {
    position: "relative",
    padding: "30px",
    backgroundColor: "#F0F0F0",
    cursor: "pointer",
    width: "100%",
    height: "150px",
    borderWidth: "2px",
    borderColor: "#C8C8C8",
    borderStyle: "dashed",
    borderRadius: "5px"
  },
  textMultiline: {
    "width": "40em",
    "wordWrap": "break-word"
  },
  pieChart: {
    "width": "20em"
  }
};

export default pathStyle;
